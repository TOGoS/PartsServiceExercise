@echo off

setlocal

if not defined psql_exe (echo Please set psql_exe as the path to psql.exe >&2 & exit /B 1)
if not defined psql_admin_username (echo Please set psql_admin_username >&2 & exit /B 1)
if not defined psql_admin_password (echo Please set psql_admin_password >&2 & exit /B 1)
if not defined pse_db_name (echo Please set pse_db_name >&2 & exit /B 1)
if not defined pse_db_host (echo Please set pse_db_host >&2 & exit /B 1)
if not defined pse_db_port (echo Please set pse_db_port >&2 & exit /B 1)

set PGPASSWORD=%psql_admin_password%

"%psql_exe%" ^
	-h "%pse_db_host%" ^
	-p "%pse_db_port%" ^
	--username="%psql_admin_username%" ^
	-v pse_db_name="%pse_db_name%" ^
	-v pse_db_username="%pse_db_username%" ^
	-f src\db-util\sql\drop-database.sql
