@echo off

setlocal
set web_url=http://localhost:%http_server_port%/

echo Welcome to....
type src\banner\txt\pse-banner.txt
echo (Dan's Parts Service Exercise)
echo:
echo If all goes well, this will create a database,
echo populate it with the contents of parts.csv and products.csv,
echo and start a web server at %web_url%
echo where you can download the data in JSON form.
echo:

echo Let's create the database!
call %~dp0\drop-database.bat
if errorlevel 1 goto fail

call %~dp0\create-database.bat
if errorlevel 1 goto fail

call %~dp0\rebuild-schema.bat
if errorlevel 1 goto fail

echo:
echo Let's run the unit tests!
call gradlew test
if errorlevel 1 goto fail

echo:
echo Let's import the data!
call gradlew importData
if errorlevel 1 goto fail

echo:
echo Let's run the web server/service!  %web_url%
call gradlew runServer
if errorlevel 1 goto fail



goto end
:fail
echo %~nx0: Oh no, something went wrong!  Aborting. >&2
exit /B 1
:end
