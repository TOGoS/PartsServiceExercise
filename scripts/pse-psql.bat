@echo off

setlocal

if not defined psql_exe (echo Please set psql_exe as the path to psql.exe >&2 & exit /B 1)
if not defined pse_db_name (echo Please set pse_db_name >&2 & exit /B 1)
if not defined pse_db_username (echo Please set pse_db_username >&2 & exit /B 1)
if not defined pse_db_password (echo Please set pse_db_password >&2 & exit /B 1)

set PGPASSWORD=%pse_db_password%

"%psql_exe%" ^
	-h "%pse_db_host%" ^
	-p "%pse_db_port%" ^
	--username="%pse_db_username%" ^
	-d "%pse_db_name%" %*
