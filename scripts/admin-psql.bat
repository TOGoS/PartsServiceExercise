@echo off

setlocal

if not defined psql_exe (echo Please set psql_exe as the path to psql.exe >&2 & exit /B 1)
if not defined psql_admin_username (echo Please set psql_admin_username >&2 & exit /B 1)
if not defined psql_admin_password (echo Please set psql_admin_password >&2 & exit /B 1)

set PGPASSWORD=%psql_admin_password%

"%psql_exe%" ^
	-h "%pse_db_host%" ^
	-p "%pse_db_port%" ^
	--username="%psql_admin_username%" ^
	%*
