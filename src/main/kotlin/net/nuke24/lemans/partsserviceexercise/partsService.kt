package net.nuke24.lemans.partsserviceexercise

import org.http4k.core.HttpHandler
import org.http4k.core.Method.GET
import org.http4k.core.Response
import org.http4k.core.Status.Companion.OK
import org.http4k.routing.bind
import org.http4k.routing.routes
import org.http4k.server.Jetty
import org.http4k.server.asServer
import kotlin.system.exitProcess

fun JSONEmitter.emit(part : Part) {
    openObject()
    emitKey("punctuatedPartNumber")
    emit(part.punctuatedPartNumber)
    emitKey("description")
    emit(part.description)
    emitKey("originalRetailPrice")
    emit(part.originalRetailPrice)
    emitKey("brandName")
    emit(part.brandName)
    emitKey("imageUrl")
    emit(part.imageUrl)
    closeObject()
}

fun JSONEmitter.emit(prod : ProductWithParts) {
    openObject()
    emitKey("productId")
    emit(prod.productId)
    emitKey("name")
    emit(prod.name)
    emitKey("categoryName")
    emit(prod.categoryName)
    emitKey("parts")
    openList()
    for (part in prod.parts) emit(part)
    closeList()
    closeObject()
}

fun JSONEmitter.emit(prods : List<ProductWithParts>) {
    openList()
    for (prod in prods) emit(prod)
    closeList()
}

fun jsonEncode(products : List<ProductWithParts>) : String {
    // If we were potentially dealing with very large lists,
    // I might provide a response body that lazily generates the JSON
    // as items are read from the database
    //
    // <acknowledgement of all the caveats and complexities that would add/>
    //
    // But for our small example database, generating a big string should be fine.
    val strBuf = StringBuilder()
    val jsonEmitter = JSONEmitter(strBuf)
    jsonEmitter.emit(products)
    // To please Dan, always end your text streams with a newline:
    jsonEmitter.closeRoot()
    return strBuf.toString()
}

class PSEWebApp(private val productAccessor:ProductAccessor) {
    fun run() {
        val app: HttpHandler = routes(
                "/" bind GET to {Response(OK).header("content-type","text/html").body(
                    "<html><head><title>Parts Service Exercise, by Dan S</title></head><body>\n" +
                    "<p>Hello, and welcome to Parts Service Exercise.</p>\n" +
                    "<p>For a product list, <a href=\"./products.json\">GET /products.json</a></p>\n" +
                    "</body></html>\n"
                ) },
                "/products.json" bind GET to { Response(OK).header("content-type", "application/json").body(
                    jsonEncode(productAccessor.getProductsWithParts())
                ) }
        )
        val portStr = System.getenv("http_server_port") ?: "8080"
        val portNum = Integer.parseInt(portStr)
        println("Starting web server on port $portNum")

        app.asServer(Jetty(portNum)).start().block()
    }
}

fun main(args: Array<String>) {
    if (!ServiceEnvironment.validateEnv()) {
        System.err.println("Some required environment variables not set; set them and try again?")
        exitProcess(1)
    }

    // https://imgflip.com/i/4ylho9
    PSEWebApp(ProductAccessor(ServiceEnvironment.makeDbConnection())).run()
}
