package net.nuke24.lemans.partsserviceexercise

fun getGreeting(): String {
    val words = mutableListOf<String>()
    words.add("Hello,")
    words.add("Ted!")

    return words.joinToString(separator = " ")
}

fun main(args: Array<String>) {
    println(getGreeting())
}
