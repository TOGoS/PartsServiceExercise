package net.nuke24.lemans.partsserviceexercise

import java.sql.DriverManager
import java.sql.Connection
import java.util.*

object EnvVars {
    const val DBNAME = "pse_db_name"
    const val DBHOST = "pse_db_host"
    const val DBPORT = "pse_db_port"
    const val USER = "pse_db_username"
    const val PASSWORD = "pse_db_password"
}

object ServiceEnvironment {
    public fun validateEnv() : Boolean {
        val requiredEnvVars = arrayOf(EnvVars.DBNAME, EnvVars.DBHOST, EnvVars.DBPORT, EnvVars.USER, EnvVars.PASSWORD)
        var anyRequiredVarsMissing = false
        for (varname in requiredEnvVars) {
            if (System.getenv(varname) == null) {
                System.err.println("Error: $varname not set")
                anyRequiredVarsMissing = true
            }
        }
        return !anyRequiredVarsMissing
    }
    public fun makeDbConnection(): Connection {
        val connectionProps = Properties()
        val dbUsername = System.getenv(EnvVars.USER)
        connectionProps.put("user", dbUsername);
        connectionProps.put("password", System.getenv(EnvVars.PASSWORD))
        Class.forName("org.postgresql.Driver")
        val connectionUrl = "jdbc:postgresql://" +
                System.getenv(EnvVars.DBHOST) + ":" +
                System.getenv(EnvVars.DBPORT) + "/" +
                System.getenv(EnvVars.DBNAME)
        System.err.println("Connecting to $connectionUrl as $dbUsername")
        return DriverManager.getConnection(connectionUrl, connectionProps)
    }
}
