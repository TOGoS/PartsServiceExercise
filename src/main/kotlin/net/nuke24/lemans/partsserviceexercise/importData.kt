package net.nuke24.lemans.partsserviceexercise

import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import java.io.File
import kotlin.system.exitProcess

fun main(args: Array<String>) {
    if (!ServiceEnvironment.validateEnv()) {
        System.err.println("Some required environment variables not set; set them and try again?")
        exitProcess(1)
    }

    val dbConn = ServiceEnvironment.makeDbConnection()

    dbConn.autoCommit = false

    val insertProductStatement = dbConn.prepareStatement("INSERT INTO pse.product (productid, productname, categoryname) VALUES (?, ?, ?)")
    // Back in my day (pre-9.5) we had to generate some monstrous update statements
    // to emulate ON CONFLICT DO NOTHING and we liked it!
    val insertPartStatement = dbConn.prepareStatement("INSERT INTO pse.part (punctuatedpartnumber, partdescription, originalretailprice, brandname, imageurl) VALUES (?, ?, ?::MONEY, ?, ?) ON CONFLICT DO NOTHING")
    val insertProductPartStatement = dbConn.prepareStatement("INSERT INTO pse.productpart (productid, punctuatedpartnumber) VALUES (?,?) ON CONFLICT DO NOTHING")

    csvReader().open(File("src/data/csv/products.csv")) {
        for (row: Map<String,String> in readAllWithHeaderAsSequence()) {
            insertProductStatement.setInt(1, Integer.parseInt(row["productId"]))
            insertProductStatement.setString(2, row["productName"])
            insertProductStatement.setString(3, row["categoryName"])
            insertProductStatement.addBatch()
        }
    }

    insertProductStatement.executeBatch()

    csvReader().open(File("src/data/csv/parts.csv")) {
        for (row: Map<String,String> in readAllWithHeaderAsSequence()) {
            insertPartStatement.setString(1, row["punctuatedPartNumber"])
            insertPartStatement.setString(2, row["partDescr"])
            insertPartStatement.setString(3, row["originalRetailPrice"])
            insertPartStatement.setString(4, row["brandName"])
            insertPartStatement.setString(5, row[""])
            insertPartStatement.addBatch()

            insertProductPartStatement.setInt(1, Integer.parseInt(row["productId"]))
            insertProductPartStatement.setString(2, row["punctuatedPartNumber"])
            insertProductPartStatement.addBatch()
        }
    }

    insertPartStatement.executeBatch()
    insertProductPartStatement.executeBatch()

    dbConn.commit()


    /*
    { reader : Cs ->
        reader.readAllWithHeadersAsSequence().forEach { row: Map<String,String> ->
            println(row)
        }
    }
     */
}
