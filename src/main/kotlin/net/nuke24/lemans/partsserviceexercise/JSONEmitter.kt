package net.nuke24.lemans.partsserviceexercise

/*
 * Editorial:
 *
 * I could have Gradle download 50MB of packages to do JSON-encoding for me, but
 *
 * A) I'm still kinda struggling with Gradle and IDEA -- the first package I added as a dependency
 *    seemed to download fine, but then the compiler couldn't find the package.
 *
 * B) Writing a JSON emitter is fun and easy
 *
 * C) This gives me something to write a unit test about
 *
 * D) You get to observe my 'emitter' pattern,
 *    which is probably called something else in some GoF book
 */

/**
 * Emits JSON to an Appendable.
 * Automatically inserting newlines, indentation, and commas as needed.
 */
class JSONEmitter(val dest:Appendable) {
    private enum class State {
        PRE_VALUE, // Ready to write a new JSON value
        OPENED, // Just emitted a { or [
        POST_VALUE, // Just emitted a value
        PRE_CLOSE, // Ready to emit a ] or }
    }
    private var state : State = State.PRE_VALUE
    private var indentLevel = 0
    private fun emitRaw(str:String) {
        dest.append(str)
    }
    private fun emitIndent() {
        for (i in 0 until indentLevel) {
            emitRaw("\t")
        }
    }
    /** Call before emitting a value to output any appropriate commas, leading indentation */
    private fun preValue() {
        when (this.state) {
            State.OPENED -> {
                emitRaw("\n")
                emitIndent()
                state = State.PRE_VALUE
            }
            State.PRE_VALUE -> {}
            State.POST_VALUE -> {
                emitRaw(",\n")
                emitIndent()
            }
            else -> throw RuntimeException("JSONEmitter#preValue is confused to be in state: $state")
        }
        state = State.PRE_VALUE
    }
    /** Call after emitting a value */
    private fun postValue() {
        state = State.POST_VALUE
    }
    private fun postOpen() {
        state = State.OPENED
        ++indentLevel
    }
    /** Call before emitting an array or list closing character */
    private fun preClose() {
        --indentLevel
        when (this.state) {
            // If any values were emitted, we'll want a new line and indent
            State.POST_VALUE -> {
                emitRaw("\n")
                emitIndent()
            }
            // Otherwise, we can just immediately close it
            State.OPENED -> {}
            else -> throw RuntimeException("JSONEmitter#preClose is confused to be in state: $state")
        }
        state = State.PRE_CLOSE
    }
    fun openObject() {
        preValue()
        emitRaw("{")
        postOpen()
    }
    fun closeObject() {
        this.preClose()
        emitRaw("}")
        postValue()
    }
    fun openList() {
        preValue()
        emitRaw("[")
        postOpen()
    }
    fun closeList() {
        preClose()
        emitRaw("]")
        postValue()
    }
    /*
     * Editorial:
     *
     * If we wanted to be cutesy, we could have emitKey return the JSONEmitter,
     * and then the caller could write emitKey("foo").emit("bar") (to emit '"foo": "bar"'),
     * keeping the code to emit the key and value on one line.
     * But since I always complain when libraries/frameworks add things for the sake
     * of 'nice syntax', I will follow my own advice and not do that.
     * "If you want to do two things, write a method that does two things!"
     * Kotlin's scope functions reduce the need for that sort of thing in other cases
     * (in general, better language design can reduce the draw of kludges for syntactic purposes,
     * which I suspect is the root cause of 90% of Java framework code).
     *
     * Though if this were C++ I'd be writing operator<<s returning references to the stream
     * because in that case it's idiomatic and it would be surprising to have it /not/ work that way.
     */
    fun emitKey(key:String) {
        preValue()
        emitStringRaw(key)
        dest.append(": ")
        state = State.PRE_VALUE
    }
    fun emitStringRaw(str:String) {
        dest.append('"')
        dest.append(str
                .replace("\\","\\\\")
                .replace("\"","\\\"")
                .replace("\b","\\b")
                .replace("\u000C","\\f")
                .replace("\n","\\n")
                .replace("\r","\\r")
                .replace("\t","\\t")
                // Backslashes, quotes, and newlines escaped = probably good enough
        )
        dest.append('"')
    }
    fun emit(strVal:String) {
        preValue()
        emitStringRaw(strVal)
        postValue()
    }
    fun emit(intVal:Int) {
        preValue()
        emitRaw(intVal.toString())
        postValue()
    }

    /** Call after a root object has been written to emit a newline
     * (all text files should end with a newline, and if you're doing JSONL
     * (https://jsonlines.org/), newlines separate the root values) */
    fun closeRoot() {
        when (state) {
            State.PRE_VALUE -> {}
            State.POST_VALUE -> emitRaw("\n")
            else -> throw RuntimeException("JSONEmitter#closeRoot is confused to be in state: $state")
        }
        state = State.PRE_VALUE
    }
}
