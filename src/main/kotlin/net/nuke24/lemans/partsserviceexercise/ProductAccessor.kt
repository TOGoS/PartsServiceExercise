package net.nuke24.lemans.partsserviceexercise

import java.sql.Connection

typealias PartID = String;
typealias ProductID = Int;

class Part(val punctuatedPartNumber : PartID, val description:String, val originalRetailPrice : String, val brandName : String, val imageUrl : String);
class ProductWithParts(val productId : ProductID, val name : String, val categoryName : String, val parts : List<Part>);

class ProductAccessor(val dbConn: Connection) {
    private fun getParts(): Map<PartID, Part> {
        val parts = HashMap<PartID, Part>()
        return dbConn.prepareStatement("SELECT * FROM pse.part").executeQuery().use() { rs ->
            while (rs.next()) {
                val partId = rs.getString("punctuatedpartnumber")
                parts[partId] = Part(
                        punctuatedPartNumber = partId,
                        description = rs.getString("partdescription"),
                        originalRetailPrice = rs.getString("originalretailprice"),
                        brandName = rs.getString("brandname"),
                        imageUrl = rs.getString("imageurl")
                )
            }
            return parts;
        }
    }

    fun getProductsWithParts(): List<ProductWithParts> {
        val productPartIds = HashMap<ProductID, ArrayList<PartID>>()

        dbConn.prepareStatement("SELECT * FROM pse.productpart").executeQuery().use() { rs ->
            while (rs.next()) {
                val productId = rs.getInt("productid")
                val partId = rs.getString("punctuatedpartnumber")
                productPartIds.getOrPut(productId, { ArrayList<PartID>() }).add(partId)
            }
        }

        val parts = getParts()

        val products = ArrayList<ProductWithParts>()
        dbConn.prepareStatement("SELECT * FROM pse.product").executeQuery().use() { rs ->
            while (rs.next()) {
                val productId = rs.getInt("productid")
                val thePartIdsForThisProduct: List<PartID> = productPartIds[productId] ?: emptyList()
                val thePartsForThisProduct = thePartIdsForThisProduct.map { id ->
                    parts[id] ?: error("Oh no, somehow we didn't have part $id")
                }
                products.add(ProductWithParts(
                        productId = productId,
                        name = rs.getString("productname"),
                        categoryName = rs.getString("categoryname"),
                        parts = thePartsForThisProduct
                ))
            }
        }

        return products
    }
}
