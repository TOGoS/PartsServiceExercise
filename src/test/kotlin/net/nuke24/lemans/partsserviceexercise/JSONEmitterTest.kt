package net.nuke24.lemans.partsserviceexercise

import kotlin.test.assertEquals
import org.junit.Test

fun getEmissionResult(emittable: ((JSONEmitter) -> Unit)): String {
    val stringBuilder = StringBuilder()
    val emitter = JSONEmitter(stringBuilder)
    emittable(emitter)
    return stringBuilder.toString()
}

class TestJSONEmitter {
    @Test
    fun testIntEncoding() {
        assertEquals("42", getEmissionResult() {
            it.emit(42)
        })
    }

    @Test
    fun testStringEncoding() {
        assertEquals("\"Bob & Tom\"", getEmissionResult() {
            it.emit("Bob & Tom")
        })
        assertEquals("\"\\\" \\\\\"", getEmissionResult() {
            it.emit("\" \\")
        }, "Quotes and backslashes should be escaped")
        assertEquals("\"\\n\\t\\\"Oh, my!\\\"\\n\"", getEmissionResult() {
            it.emit("\n\t\"Oh, my!\"\n")
        }, "Tab characters should get escaped")
        // This one's to make sure string.replace actually replaces /all/ occurrences:
        assertEquals("\"\\\"\\\\\\b\\f\\n\\r\\t \\\"\\\\\\b\\f\\n\\r\\t\"", getEmissionResult() {
            it.emit("\"\\\b\u000C\n\r\t \"\\\b\u000C\n\r\t")
        }, "All the single-character backslash escapes should work...twice")
    }

    @Test
    fun testObjectEncoding() {
        assertEquals("{\n\t\"foo\": \"bar\"\n}", getEmissionResult() {
            it.openObject()
            it.emitKey("foo")
            it.emit("bar")
            it.closeObject()
        })
    }

    @Test
    fun testEmptyObjectEncoding() {
        assertEquals("{}", getEmissionResult() {
            it.openObject()
            it.closeObject()
        })
    }

    @Test
    fun testEmptyListEncoding() {
        assertEquals("[]", getEmissionResult() {
            it.openList()
            it.closeList()
        })
    }

    @Test
    fun testMoreComplicatedObjectEncoding() {
        assertEquals("{\n\t\"a list\": [\n\t\t1,\n\t\t2,\n\t\t3\n\t],\n\t\"foo\": \"bar\"\n}", getEmissionResult() {
            it.openObject()
            it.emitKey("a list")
            it.openList()
            it.emit(1)
            it.emit(2)
            it.emit(3)
            it.closeList()
            it.emitKey("foo")
            it.emit("bar")
            it.closeObject()
        })
    }
}
