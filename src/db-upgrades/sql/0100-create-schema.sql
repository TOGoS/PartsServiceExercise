CREATE SCHEMA pse; -- 'Parts Service Exercise'

CREATE TABLE pse.product (
  productid INTEGER NOT NULL,
  productname TEXT NOT NULL,
  categoryname TEXT NOT NULL,
  PRIMARY KEY (productid)
);

CREATE TABLE pse.part (
  punctuatedpartnumber TEXT NOT NULL,
  partdescription TEXT NOT NULL,
  originalretailprice MONEY NOT NULL,
  brandname TEXT NOT NULL,
  imageurl TEXT NOT NULL,
  PRIMARY KEY (punctuatedpartnumber)
);

CREATE TABLE pse.productpart (
  productid INTEGER NOT NULL,
  punctuatedpartnumber TEXT NOT NULL,
  PRIMARY KEY (productid, punctuatedpartnumber),
  FOREIGN KEY (productid) REFERENCES pse.product (productid),
  FOREIGN KEY (punctuatedpartnumber) REFERENCES pse.part (punctuatedpartnumber)
);
