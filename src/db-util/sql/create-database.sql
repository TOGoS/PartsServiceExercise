-- Somebody once told me
--   As of psql 9.1, variables can be expanded in quotes as in:
--     \set myvariable value 
--     SELECT * FROM table1 WHERE column1 = :'myvariable';
-- (https://stackoverflow.com/questions/36959/how-do-you-use-script-variables-in-psql)

CREATE DATABASE :"pse_db_name";
CREATE USER :"pse_db_username" WITH PASSWORD :'pse_db_password';
GRANT ALL PRIVILEGES ON DATABASE :"pse_db_name" TO :"pse_db_username";
